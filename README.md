# IoTprojekt

<h4>repozitorij projekta DonIote</h4>

<p>sadržana je najnovija verzija dokumentacije i koda korištenog i nodered flow-ova</p>
<p>U truffle.rar nije sadržan node modules nego je potrebno npm install napraviti.</p>
<p>da bi java kod funkcionirao potrebno je koristiti specifično java 1.8 jdk</p>
<p>Potrebno je dodati sadržaje datoteka core.entity_registry, input_number i input_boolean u istoimene datoteke u direktoriju /config/.storage kako bi helperi bili dodani</p>
<p>automations.yaml se treba kopirati u /config direktorij</p>
<p>Nakon spajanja na mrežu, Home Assistantu se pristupa preko adrese homeassistant.local:8123/</p>
<p>Node Red dodatak se može instalirati putem add-on storea, a flowovi se trebaju importati iz datoteke NodeRED/flows.txt</p>