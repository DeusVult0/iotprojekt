var express = require('express')
var app = express()
var http = require('http');
var Web3 = require("web3");
var contract = require('@truffle/contract');
var provider = new Web3.providers.HttpProvider('HTTP://127.0.0.1:7545');
let web3 = new Web3(provider);
var SenzorCoinJson = require("./build/contracts/SenzorCoin.json");
var SenzorCoin = contract(SenzorCoinJson);
SenzorCoin.setProvider(provider);
var dopustenje = false;

function setDopustenje() {
  dopustenje = false;
}



// NEMOJ ZABORAVITI PRVO MIGRIRATI, A ONDA POKRENUTI NODE.JS
app.post('/reading', (req, res) => {
  console.log("Primljeno očitanje")
  if (dopustenje == true) {
    res.send("IMAŠ DOPUŠTENJE")
    var options = {
      host: 'homeassistant.local',
      path: '/api/webhook/presence_detected',
      port: '8123',
      method: 'POST',
      
    };
    
    callback = function(response) {

      return;
    }
   
    var req = http.request(options, callback);
    req.on('error', (e) => {
      console.error('ERROR IGNORED');
    });
    req.end();
    
  }
  else {
    res.send("NEMAŠ DOPUŠTENJE");
  }


})


app.post('/', (req, res) => {

    SenzorCoin.deployed().then(function (instance) {
      return instance.ownerOf(0);
      }).then(function (owner) {
        var recoveredAddress = null;
        try {
          recoveredAddress = web3.eth.accounts.recover('homeAssistantProjektR',req.headers.data);
        console.log(recoveredAddress)
        } catch (error) {     
          
        }
        if (recoveredAddress == owner ) {
          console.log("Šaljem dopuštenje na homeassistant.")
          dopustenje = true;
          setTimeout(setDopustenje, 30000);
          res.send("OK");
        }        
        else {
          console.log("Autorizacija neuspjela, molimo pokušajte ponovo.")
          res.send("NOT OK");
        }


      
      });
  })


app.listen(3000);




