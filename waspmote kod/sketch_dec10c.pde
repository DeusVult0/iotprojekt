#include <WaspSensorEvent_v30.h>
#include <WaspFrame.h> 
#include <WaspXBee802.h>

uint8_t value = 0;
// Destination MAC address
//////////////////////////////////////////
char rx_address[] = "0013A20041951890";
//////////////////////////////////////////

// Define the Waspmote ID
char waspmote_id[] = "KOMP";

uint8_t error;

pirSensorClass pir(SOCKET_1);

void setup() 
{

  // init USB port
  USB.ON();
  USB.println(F("usb upaljen"));

  frame.setID(waspmote_id);
  
  // store Waspmote identifier in EEPROM memor
  xbee802.ON();
  
  USB.println(F("802 upaljen"));
  delay(1000);

  USB.println(F("-------------------------------")); 
  
  // Turn on the sensor board
  Events.ON();
    
  // Firstly, wait for PIR signal stabilization
  value = pir.readPirSensor();
  while (value == 1)
  {
    USB.println(F("...wait for PIR stabilization"));
    delay(1000);
    value = pir.readPirSensor();    
  }
  
  // Enable interruptions from the board
  Events.attachInt();
}

void loop() 
{
  // Read the PIR Sensor
  value = pir.readPirSensor();
  
  ///////////////////////////////////////
  // 2. Go to deep sleep mode
  ///////////////////////////////////////
  USB.println(F("enter deep sleep"));
  PWR.deepSleep("00:00:00:10", RTC_OFFSET, RTC_ALM1_MODE1, SENSOR_ON);
  USB.ON();
  xbee802.ON();
  USB.println(F("wake up\n"));  
  
  ///////////////////////////////////////
  // 3. Check Interruption Flags
  ///////////////////////////////////////

  
  // 3.1. Check interruption from RTC alarm
  if (intFlag & RTC_INT)
  {
    USB.println(F("-----------------------------"));
    USB.println(F("RTC INT captured"));
    USB.println(F("-----------------------------"));

    // clear flag
    intFlag &= ~(RTC_INT);
  }
  
  // 3.2. Check interruption from Sensor Board
  if (intFlag & SENS_INT)
  {
    // Disable interruptions from the board
    Events.detachInt();
    
    // Load the interruption flag
    Events.loadInt();
    
    // In case the interruption came from PIR
    if (pir.getInt())
    {
      USB.println(F("-----------------------------"));
      USB.println(F("Interruption from PIR"));
      USB.println(F("-----------------------------"));
      frame.createFrame(ASCII);
      frame.addSensor(SENSOR_STR, "Prisutnost detektirana");
      error = xbee802.send(rx_address, frame.buffer, frame.length);
      // check TX flag
      if( error == 0 )
      {
        USB.println(F("send ok"));
    
       // blink green LED
      Utils.blinkGreenLED();
    
      }
      else 
      {
        USB.println(F("send error"));
    
      // blink red LED
      Utils.blinkRedLED();
      }
      delay(1000);
    }    
       
  
  
    // User should implement some warning
    // In this example, now wait for signal
    // stabilization to generate a new interruption
    // Read the sensor level
    value = pir.readPirSensor();
    
    while (value == 1)
    {
      USB.println(F("...wait for PIR stabilization"));
      delay(1000);
      value = pir.readPirSensor();
    }
    
    // Clean the interruption flag
    intFlag &= ~(SENS_INT);
    
    // Enable interruptions from the board
    Events.attachInt();
  }  
}
