package com.digi.xbee.api.receivedata;

import java.net.*;
import java.io.*;
import java.nio.*;

public class SendWebHook {
	public static byte[] data;
	public static HttpURLConnection con;
	
	public static void uspostaviVezu(byte[] data) throws Exception {
		//URL url = new URL("http://homeassistant.local:8123/api/webhook/presence_detected");
		URL url = new URL("http://localhost:3000/reading");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setDoOutput(true);
		con.setRequestMethod("POST");
		con.setConnectTimeout(10000);
		/*try(DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
			wr.write(data);
		}*/
		int status = con.getResponseCode();
		System.out.println(status);
	}
	
	public SendWebHook(byte[] data) {
		this.data = data;
		try {
			uspostaviVezu(data);
		} catch(Exception e) {
			System.out.println(e);
		} finally {
			con.disconnect();
		}
	}
}
